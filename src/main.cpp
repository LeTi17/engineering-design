#include <Arduino.h>
#include <Wire.h>
#include <XT_DAC_Audio.h>
#include "Rfid_RC552.h"
#include "Display_SSD1327.h"
#include "GSM_SIM800L.h"
#include "Sound_Data.h"

/* Use this Snippet instead of Delay
  uint32_t currentMillis = millis();
  while(millis() - currentMillis <= miliseconds);*/

//Active State of the ESP32 - calling/displaying time/ sending SMS/ audio (in s)
#define ACTIVE_TIME_STATE 240

#define PRESET_START_TIME "20/10/16,18:32:00+08"
#define PRESET_END_TIME "20/10/16,18:34:00+08"
#define REED_RELAY_PIN 33

uint32_t timeStamp_READ;

//Flags Section
bool cross_displayed = false;
bool tick_displayed = false;
bool after_rfid_read_init = false;
bool alert_was_send = false;
bool door_opened = false;

//Compare current time with the time interval
bool time_IsRight(String preset_start, String preset_end, String current_time)
{

  if (current_time >= preset_start && current_time <= preset_end)
    return false;

  else
    return true;
}

XT_Wav_Class Alert_Audio(han_solo);

XT_DAC_Audio_Class DacAudio(25, 0);

/*void play_alert()
{ 
  int i = 0;

  uint32_t currentMillis = millis();
  while(millis() - currentMillis <= 2000);
  
  for(i = 0; i < 5; i++)
  {
    DacAudio.FillBuffer();
    DacAudio.PlayWav(&Alert_Audio);
  }

  currentMillis = millis();
  while(millis() - currentMillis <= 4000);
  
  DacAudio.FillBuffer();
  DacAudio.PlayWav(&Alert_Audio);
}*/

//RESET Functions
void reset_icons()
{
  u8g2.clearBuffer();
  cross_displayed = false;
  tick_displayed = false;
}

void RFID_Display_Mode()
{
  displayMessage("Waiting for RFID Tag...");
  drawNFC();
}

void Reinit_RFID_Display_Mode()
{
  auth_RFID = false;
  after_rfid_read_init = false;
  alert_was_send = false;
  reset_icons();
  RFID_Display_Mode();
  SPI.begin();
}
//END RESET Functions

void setup(void)
{
  //Initialize Dsiplay + RFID Module + Set Reed Relay PIN
  init_RFID();
  init_Display();
  RFID_Display_Mode();
  pinMode(REED_RELAY_PIN, INPUT_PULLUP);
}

void loop()
{
  check_RFID();

  if (auth_RFID == true)
  {

    if (after_rfid_read_init == false)
    {
      SPI.end();
      init_SIM800L();
      after_rfid_read_init = true;
      timeStamp_READ = millis();
    }

    while (millis() - timeStamp_READ <= ACTIVE_TIME_STATE * S_TO_mS_FACTOR)
    {
      get_TIME();

      if (time_IsRight(PRESET_START_TIME, PRESET_END_TIME, modem.getGSMDateTime(DATE_FULL)) == true)
      {

        displayTime(timeinfo.hour, timeinfo.minute);
        if (tick_displayed == false)
        {
          drawTick();
          tick_displayed = true;
        }
      }

      else
      {
        displayTime(timeinfo.hour, timeinfo.minute);

        if (cross_displayed == false)
        {
          drawCross();
          cross_displayed = true;
        }

        if (digitalRead(REED_RELAY_PIN) == HIGH)
          door_opened = true;

        if (alert_was_send == false && door_opened == true)
        {
          alert_animation(4);
          call_TARGET(2);
          uint32_t currentMillis = millis();
          while (millis() - currentMillis <= 100)
            ;
          //send_SMS();
          alert_was_send = true;
          door_opened = false;
          reset_icons();
        }
      }
    }
    Reinit_RFID_Display_Mode();
  }
}
