# **Notes**
&rarr;&rarr;&rarr; **This file is intended for keeping notes related to the project** &larr;&larr;&larr;

### **1. Lebara Settings**
```
Email: lebara@bboygarage.com  
Password: *LebaraTTGO1234*  
Lebara TTGO: +31620436215  
Lebara TEST: +31620198599  
APN: portalmmm.nl
```

### **2. GPIOs Modem**
```
MODEM_PWKEY = 4
MODEM_RST = 5
MODEM_POWER_ON = 23
```

### **RFID Bracelet Address**
```
F4 96 8E 3F
```
### **RFID Card Address**
```
23 E2 B4 3E
```
