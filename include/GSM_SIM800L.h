//TinyGsmLibrary Configuration
#define TINY_GSM_MODEM_SIM800
#define TINY_GSM_RX_BUFFER 1024

//NO need for SIM PIN

//Phone number for sending SMS and Calling
const char TARGET_NUMBER[] = "+31620198599";

#include <TinyGsmClient.h>

// TTGO T-Call pins
#define MODEM_RST 5
#define MODEM_PWKEY 4
#define MODEM_POWER_ON 23
#define MODEM_TX 27
#define MODEM_RX 26

// Set serial for debug console (to Serial Monitor, default speed 115200)
#define SerialMon Serial
// Set serial for AT commands (to SIM800 module)
#define SerialAT Serial1

#define BAUD_RATE 115200

//TinyGsm Modem Instance
TinyGsm modem(SerialAT);

//TinyGsm Client for Internet Connection
TinyGsmClient client(modem);

#define uS_TO_S_FACTOR 1000000 // Conversion factor for micro seconds to seconds //
#define S_TO_mS_FACTOR 1000 // Conversion factor for seconds to miliseconds //

//Variables to store the GSM TimeStamp
struct tM{

int hour;
int minute;
int second;

}timeinfo;

void init_SIM800L()
{
    //Initialize the Serial Monitor
    SerialMon.begin(BAUD_RATE);
    delay(10);

    //Set Proper State of Pins
    pinMode(MODEM_PWKEY, OUTPUT);
    pinMode(MODEM_RST, OUTPUT);
    pinMode(MODEM_POWER_ON, OUTPUT);
    digitalWrite(MODEM_PWKEY, LOW);
    digitalWrite(MODEM_RST, HIGH);
    digitalWrite(MODEM_POWER_ON, HIGH);

    //Initialize Serial Connection with the SIM800L Module
    SerialAT.begin(BAUD_RATE, SERIAL_8N1, MODEM_RX, MODEM_TX);

    //SerialMon.println("Initializing Modem....");
    displayMessage("Initializing Modem....");
    drawSetup();

    //Connect to GSM and wait 7.5 sec
    modem.init();
    uint32_t currentMillis = millis();
    while(millis() - currentMillis <= 7500);

    //SerialMon.println("----Initialization Finished----");
    displayMessage("Connection Established!");
    drawThumbUP();

    //Wait for another 2.5 sec
    currentMillis = millis();
    while(millis() - currentMillis <= 2500);

    u8g2.clearBuffer();

}

//Function to send and SMS to the Target Number
void send_SMS()
{
    String smsMessage = "$$Message From TTGO$$";

    displayMessage("Sending SMS...");
    drawSMS();

    if (modem.sendSMS(TARGET_NUMBER, smsMessage))
        SerialMon.println("SMS:" + smsMessage + "was send succesfully!");

    else
        SerialMon.println("Failed to send SMS");
}

//Function to call the Target Number
void call_TARGET(int repeat_nr)
{
    int i = 0;

    uint32_t currentMillis = millis();
    while(millis() - currentMillis <= 1500);
    
    displayMessage("Calling Caregiver...");
    drawPhoneCall();

    for (i = 0; i < repeat_nr; i++)
    {
        modem.callNumber(TARGET_NUMBER);
        
        uint32_t currentMillis = millis();
        while(millis() - currentMillis <= 20000);
        
        modem.callHangup();
        currentMillis = millis();
        while(millis() - currentMillis <= 1000);
    }
}

void get_TIME()
{
   const char* t_Network_STAMP = modem.getGSMDateTime(DATE_TIME).c_str();
   sscanf(t_Network_STAMP, "%d:%d:%d", &timeinfo.hour, &timeinfo.minute, &timeinfo.second);
}

//Update Serial with INFO from AT Commands
void update_Serial()
{
    if (SerialAT.available())
        SerialMon.write(SerialAT.read());
        
    if (SerialMon.available())
        SerialAT.write(SerialMon.read());
}
